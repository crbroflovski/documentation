#!/bin/bash

if [ "root" != "$USER" ]; then
	echo -en "\ec\e[3J"
	echo -e '--> Forneça a senha de acesso root do sistema para continuar:'
	su -c "$0" root
	exit
fi

function atualizar (){
	echo -en "\ec\e[3J"
	echo -e "--> Atualizando repositório..."
	sleep 3
	apt-get update
}

function instalar () {
	echo ""
	echo -e "########## INSTALAÇÃO AUTOMÁTICA DE PROGRAMAS ##########"
	echo -e "->Deseja instalar: $1 ? (y/n)"
	while true; do
		read opcao
		case $opcao in
			y) apt install $1
			   break;;
			n) break;;
			*) echo "ERRO: Você tem de entrar com um parâmetro válido (y ou n)" ;;
		esac
	done
}

function finalizar (){
	echo ""
	echo -e "Obrigado por utilizar o script!"
	sleep 3
	echo -n "Script fechando em:"
	sleep 1
	echo -n "  3"
	sleep 1
	echo -n "  2"
	sleep 1
	echo -n "  1"
	sleep 1
	echo ""
}

atualizar
instalar "i3-wm"
instalar "i3status"
instalar "dmenu"
instalar "i3lock"
instalar "feh"
instalar "apache2 libapache2-mod-wsgi"
instalar "mysql-server mysql-client"
instalar "postgresql"
instalar "python-setuptools python-dev build-essential python-pip libpq-dev libmysqlclient-dev python-mysqldb"
instalar "php5 libapache2-mod-php5 php5-curl php5-gd"
instalar "phpmyadmin"
instalar "phppgadmin"
instalar "virtualenv"
instalar "git"
instalar "openssh-server openssh-client"
instalar "flashplugin-nonfree"
instalar "htop"
instalar "transmission"
instalar "vim gedit"
finalizar
